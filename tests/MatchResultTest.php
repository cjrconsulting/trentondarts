<?php

use TrentonDarts\MatchDomain\Models\GamePlayer;
use TrentonDarts\MatchDomain\Models\GameRules;
use TrentonDarts\MatchDomain\Models\MatchResult;
use TrentonDarts\MatchDomain\Models\MatchRules;

class MatchResultTest extends TestCase
{
    static $gameRuleId = 1;

    private static function getGameRuleForWinter801()
    {
        $rules = new GameRules();
        $rules->numberOfLegs = 1;
        $rules->numberOfPlayers = 3;
        $rules->doubleIn = false;
        $rules->doubleOut = true;
        $rules->id = self::$gameRuleId;
        $rules->gameType = "801";
        $rules->gamePointValue = 3;

        self::$gameRuleId++;

        return $rules;
    }

    private static function getGameRuleForWinter501()
    {
        $rules = new GameRules();
        $rules->numberOfLegs = 1;
        $rules->numberOfPlayers = 2;
        $rules->doubleIn = false;
        $rules->doubleOut = true;
        $rules->id = self::$gameRuleId;
        $rules->gameType = "501";
        $rules->gamePointValue = 2;

        self::$gameRuleId++;

        return $rules;
    }

    private static function getGameRuleForWinter301()
    {
        $rules = new GameRules();
        $rules->numberOfLegs = 1;
        $rules->numberOfPlayers = 1;
        $rules->doubleIn = true;
        $rules->doubleOut = true;
        $rules->id = self::$gameRuleId;
        $rules->gameType = "301";
        $rules->gamePointValue = 1;

        self::$gameRuleId++;

        return $rules;
    }

    private static function getGameRuleForWinterCricket()
    {
        $rules = new GameRules();
        $rules->numberOfLegs = 1;
        $rules->numberOfPlayers = 1;
        $rules->id = self::$gameRuleId;
        $rules->gameType = "cricket";
        $rules->gamePointValue = 1;

        self::$gameRuleId++;

        return $rules;
    }

    private static function getGameRuleForWinterDoublesCricket()
    {
        $rules = new GameRules();
        $rules->numberOfLegs = 1;
        $rules->numberOfPlayers = 2;
        $rules->id = self::$gameRuleId;
        $rules->gameType = "cricket";
        $rules->gamePointValue = 2;

        self::$gameRuleId++;

        return $rules;
    }

    public function testLoadingRulesWillBeTheRules()
    {
        $matchResult = new MatchResult();
        $matchRules = new MatchRules();
        array_push($matchRules->gameRules, new GameRules());
        $matchResult->loadRules($matchRules);

        $this->assertEquals($matchRules, $matchResult->getRules());
    }

    public function testLoadingRulesWillHaveTheSameNumberOfGames()
    {
        $matchResult = new MatchResult();
        $matchRules = new MatchRules();
        array_push($matchRules->gameRules, new GameRules());
        $matchResult->loadRules($matchRules);

        $this->assertEquals(sizeof($matchRules->gameRules), sizeof($matchResult->getGames()));
    }

    public function testGetHomeTotalScore()
    {
        $matchResult = $this->CreateStandardMatchWithSweep("home");
        $this->assertEquals(27, $matchResult->getHomeScore());
    }

    public function testGetHomeTotalScoreWithSweep()
    {
        $matchResult = $this->CreateStandardMatchWithSweep("away");
        $this->assertEquals(27, $matchResult->getAwayScore());
    }

    public function getPlayer($id, $name)
    {
        $player = new GamePlayer();
        $player->id = $id;
        $player->name = $name;
        return $player;
    }

    /**
     * @return MatchResult
     */
    private function CreateStandardMatch()
    {
        $matchResult = new MatchResult();
        $matchRules = new MatchRules();
        array_push($matchRules->gameRules, self::getGameRuleForWinter301());
        array_push($matchRules->gameRules, self::getGameRuleForWinter301());
        array_push($matchRules->gameRules, self::getGameRuleForWinter301());
        array_push($matchRules->gameRules, self::getGameRuleForWinterCricket());
        array_push($matchRules->gameRules, self::getGameRuleForWinterCricket());
        array_push($matchRules->gameRules, self::getGameRuleForWinterCricket());
        array_push($matchRules->gameRules, self::getGameRuleForWinter501());
        array_push($matchRules->gameRules, self::getGameRuleForWinter501());
        array_push($matchRules->gameRules, self::getGameRuleForWinter501());
        array_push($matchRules->gameRules, self::getGameRuleForWinter801());
        array_push($matchRules->gameRules, self::getGameRuleForWinterDoublesCricket());
        array_push($matchRules->gameRules, self::getGameRuleForWinterDoublesCricket());
        array_push($matchRules->gameRules, self::getGameRuleForWinterDoublesCricket());
        array_push($matchRules->gameRules, self::getGameRuleForWinterCricket());
        array_push($matchRules->gameRules, self::getGameRuleForWinterCricket());
        array_push($matchRules->gameRules, self::getGameRuleForWinterCricket());
        array_push($matchRules->gameRules, self::getGameRuleForWinter301());
        array_push($matchRules->gameRules, self::getGameRuleForWinter301());
        array_push($matchRules->gameRules, self::getGameRuleForWinter301());

        $matchResult->loadRules($matchRules);
        return $matchResult;
    }

    /**
     * @param $teamType
     * @return MatchResult
     */
    private function CreateStandardMatchWithSweep($teamType)
    {
        $matchResult = $this->CreateStandardMatch();

        $matchResult->getGames()[0]->addLeg(1, $teamType);
        $matchResult->getGames()[1]->addLeg(1, $teamType);
        $matchResult->getGames()[2]->addLeg(1, $teamType);

        $matchResult->getGames()[3]->addLeg(1, $teamType);
        $matchResult->getGames()[4]->addLeg(1, $teamType);
        $matchResult->getGames()[5]->addLeg(1, $teamType);

        $matchResult->getGames()[6]->addLeg(1, $teamType);
        $matchResult->getGames()[7]->addLeg(1, $teamType);
        $matchResult->getGames()[8]->addLeg(1, $teamType);

        $matchResult->getGames()[9]->addLeg(1, $teamType);

        $matchResult->getGames()[10]->addLeg(1, $teamType);
        $matchResult->getGames()[11]->addLeg(1, $teamType);
        $matchResult->getGames()[12]->addLeg(1, $teamType);

        $matchResult->getGames()[13]->addLeg(1, $teamType);
        $matchResult->getGames()[14]->addLeg(1, $teamType);
        $matchResult->getGames()[15]->addLeg(1, $teamType);

        $matchResult->getGames()[16]->addLeg(1, $teamType);
        $matchResult->getGames()[17]->addLeg(1, $teamType);
        $matchResult->getGames()[18]->addLeg(1, $teamType);
        return $matchResult;
    }
}