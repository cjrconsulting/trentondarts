<?php

use TrentonDarts\MatchDomain\Models\GameRules;
use TrentonDarts\MatchDomain\Models\GamePlayer;
use TrentonDarts\MatchDomain\Models\GameResult;

class GameResultTest extends TestCase
{
    /**
     *
     */
    public function testAddingAPlayer_IsAvailableOnGame(){
        $gameRules = new GameRules();
        $gameRules->numberOfPlayers = 1;
        $gameResult = new GameResult($gameRules);

        $playerOne = $this->getPlayer(1, "First Player");
        $playerTwo = $this->getPlayer(2, "Second Player");

        $gameResult->setHomePlayer($playerOne, 1);
        $gameResult->setAwayPlayer($playerTwo, 1);

        $snapShot = $gameResult->getSnapshot();
        $this->assertEquals(1, sizeof($snapShot->homePlayers));
        $this->assertEquals(1, sizeof($snapShot->awayPlayers));
    }

    public function testRemovingAHomePlayer_IsNoLongerOnGame()
    {
        $gameRules = new GameRules();
        $gameRules->numberOfPlayers = 2;
        $gameResult = new GameResult($gameRules);

        $playerOne = $this->getPlayer(1, "First Player");
        $playerTwo = $this->getPlayer(2, "Second Player");
        $playerThree = $this->getPlayer(3, "Third Player");

        $gameResult->setHomePlayer($playerOne, 1);
        $gameResult->setAwayPlayer($playerTwo, 1);
        $gameResult->setHomePlayer($playerThree, 2);

        $gameResult->removeHomePlayer($playerThree);

        $snapshot = $gameResult->getSnapshot();
        $this->assertEquals(1, sizeof($snapshot->homePlayers));
    }

    public function testRemovingAnAwayPlayer_IsNoLongerOnGame()
    {
        $gameRules = new GameRules();
        $gameRules->numberOfPlayers = 2;
        $gameResult = new GameResult($gameRules);

        $playerOne = $this->getPlayer(1, "First Player");
        $playerTwo = $this->getPlayer(2, "Second Player");
        $playerThree = $this->getPlayer(3, "Third Player");

        $gameResult->setHomePlayer($playerOne, 1);
        $gameResult->setAwayPlayer($playerTwo, 1);
        $gameResult->setAwayPlayer($playerThree,2);

        $gameResult->removeAwayPlayer($playerThree);

        $snapshot = $gameResult->getSnapshot();
        $this->assertEquals(1, sizeof($snapshot->awayPlayers));
    }

    /**
     * @expectedException TrentonDarts\MatchDomain\Models\TooManyPlayersException
     */
    public function testAddingToManyHomePlayers_ThrowsException()
    {
        $gameRules = new GameRules();
        $gameRules->numberOfPlayers = 1;
        $gameResult = new GameResult($gameRules);

        $playerOne = $this->getPlayer(1, "First Player");
        $playerTwo = $this->getPlayer(2, "Second Player");

        $gameResult->setHomePlayer($playerOne, 1);
        $gameResult->setHomePlayer($playerTwo, 1);
    }

    /**
     * @expectedException TrentonDarts\MatchDomain\Models\TooManyPlayersException
     */
    public function testAddingToManyAwayPlayers_ThrowsException()
    {
        $gameRules = new GameRules();
        $gameRules->numberOfPlayers = 1;
        $gameResult = new GameResult($gameRules);

        $playerOne = $this->getPlayer(1, "First Player");
        $playerTwo = $this->getPlayer(2, "Second Player");

        $gameResult->setAwayPlayer($playerOne, 1);
        $gameResult->setAwayPlayer($playerTwo, 1);
    }

    public function testWhenAddingLegResult_ItIsEvailableOnSnapshot()
    {
        $gameRules = new GameRules();
        $gameRules->numberOfPlayers = 1;
        $gameRules->numberOfLegs = 1;
        $gameResult = new GameResult($gameRules);

        $gameResult->addLeg(1, "Home");

        $result = $gameResult->getSnapshot();

        $this->assertEquals(1, sizeof($result->legs));
    }

    /**
     * @expectedException TrentonDarts\MatchDomain\Models\TooManyLegsException
     */
    public function testWhenAddingLegToManyResult_ThrowException()
    {
        $gameRules = new GameRules();
        $gameRules->numberOfPlayers = 1;
        $gameRules->numberOfLegs = 1;
        $gameResult = new GameResult($gameRules);

        $gameResult->addLeg(1, "Home");
        $gameResult->addLeg(2, "Away");
    }

    public function testWhenGameHasPointValueAndNoBestOf_TheHomeScoreShouldBeOne()
    {
        $gameRules = new GameRules();
        $gameRules->numberOfPlayers = 1;
        $gameRules->numberOfLegs = 1;
        $gameRules->gamePointValue = 1;

        $gameResult = new GameResult($gameRules);
        $gameResult->setHomePlayer($this->getPlayer(1, "Player 1"), 1);
        $gameResult->setAwayPlayer($this->getPlayer(2, "Player 2"), 1);
        $gameResult->addLeg(1, "home");

        $result = $gameResult->getHomeScore();
        $this->assertEquals(1, $result);
    }

    public function testWhenGameHasPointValueAndNoBestOf_TheAwayScoreShouldBeOne()
    {
        $gameRules = new GameRules();
        $gameRules->numberOfPlayers = 1;
        $gameRules->numberOfLegs = 1;
        $gameRules->gamePointValue = 1;

        $gameResult = new GameResult($gameRules);
        $gameResult->setHomePlayer($this->getPlayer(1, "Player 1"), 1);
        $gameResult->setAwayPlayer($this->getPlayer(2, "Player 2"), 1);
        $gameResult->addLeg(1, "away");

        $result = $gameResult->getAwayScore();
        $this->assertEquals(1, $result);
    }

    public function testGamePointValueIsUsedForScore(){
        $gameRules = new GameRules();
        $gameRules->numberOfPlayers = 1;
        $gameRules->numberOfLegs = 1;
        $gameRules->gamePointValue = 3;

        $gameResult = new GameResult($gameRules);
        $gameResult->setHomePlayer($this->getPlayer(1, "Player 1"), 1);
        $gameResult->setAwayPlayer($this->getPlayer(2, "Player 2"), 1);
        $gameResult->addLeg(1, "away");

        $result = $gameResult->getAwayScore();
        $this->assertEquals(3, $result);
    }

    public function getPlayer($id, $name)
    {
        $player = new GamePlayer();
        $player->id = $id;
        $player->name = $name;
        return $player;
    }
}
