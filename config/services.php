<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        //'domain' => 'sandboxb1cec11ec914421a9aefc6cd9a4417ac.mailgun.org',
        'domain' => 'mg.trentondarts.com',
        'secret' => 'key-664bf3debb055caa5d5fb2d6aa699716',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => TrentonDarts\User::class,
        'key'    => '',
        'secret' => '',
    ],

];
