'use strict';
var gulp = require('gulp');
var notify = require('gulp-notify');
var Elixir = require('laravel-elixir');
var eslint = require('gulp-eslint');

var Task = Elixir.Task;

Elixir.extend('eslint', function(src, options) {
    var paths = src || [
            config.get('public.js.outputFolder') + '/**/*.js',
            '!' + config.get('public.js.outputFolder') + '/vendor/**/*.js'
        ];

    new Task('eslint', function () {
        this.log(paths);

        return gulp.src(paths)
            .pipe(eslint(options || {}))
            .pipe(eslint.format())
            .pipe(eslint.failAfterError())
            .on('error', function (e) {
                notify.onError({
                    title: this.title,
                    message: 'ESLint Failed!' + ': <%= error.message %>'
                })(e);
                this.emit('end');
            });
    }).watch(paths);
});