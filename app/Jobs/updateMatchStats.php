<?php

namespace TrentonDarts\Jobs;

use Exception;
use TrentonDarts\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use TrentonDarts\LeagueManagement\Models\WinterSeasonMatch;
use TrentonDarts\MatchDomain\MatchRepository;
use TrentonDarts\MatchDomain\Models\MatchResult;
use TrentonDarts\Stats\AwardStatRepository;
use TrentonDarts\Stats\MatchStatsRepository;
use TrentonDarts\Stats\PlayerGameRepository;
use TrentonDarts\Stats\TeamGameRepository;

class updateMatchStats extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $matchId;

    /**
     * Create a new job instance.
     *
     * @param Int|Integer $matchId
     */
    public function __construct($matchId)
    {
        $this->matchId = $matchId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $match = WinterSeasonMatch::findOrFail($this->matchId);
            $matchResults = MatchRepository::getMatchResultsFromMatch($match);
            $matchStatRepository = new MatchStatsRepository();
            $matchStatRepository->updateMatchStats($matchResults);

            $teamGameStatRepository = new TeamGameRepository();
            $teamGameStatRepository->updateTeamGameStats($matchResults);

            $playerGameStatRepository = new PlayerGameRepository();
            $playerGameStatRepository->updatePlayerGameStats($matchResults);

            $awardStatRepository = new AwardStatRepository();
            $awardStatRepository->updateAwardStats($matchResults);
        } catch(Exception $ex){
            echo $ex->getMessage();
            var_dump($ex->getTrace());
            throw $ex;
        }
    }
}
