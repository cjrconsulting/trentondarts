<?php

namespace TrentonDarts\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('shared.defaultnav',
            'TrentonDarts\Http\ViewComposers\NavComposer');
        view()->composer('season.winter.standings',
            'TrentonDarts\Http\ViewComposers\StandingsComposer');
        view()->composer('shared.eventlist',
            'TrentonDarts\Http\ViewComposers\DartEventsComposer');
        view()->composer('season.winter.nextschedule',
            'TrentonDarts\Http\ViewComposers\NextScheduleComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
