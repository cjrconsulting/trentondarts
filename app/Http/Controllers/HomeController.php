<?php

namespace TrentonDarts\Http\Controllers;

use TrentonDarts\SiteManagement\Models\DartEvent;
use TrentonDarts\SiteManagement\Models\PagePart;

class HomeController extends Controller
{
    public function index()
    {
        $titleEvent = DartEvent::where('isTitleEvent', true)->first();
        if ($titleEvent == null) {
            $titleEvent = DartEvent::where('eventDate', '>=', date('Y-m-d'))->orderBy('eventDate', 'asc')->first();
        }

        $pageParts = PagePart::where('name', 'MainPageHeader')->get();
        $welcomeMessage = '';
        if (!$pageParts->isEmpty()) {
            $welcomeMessage = $pageParts->first()->html;
        }

        $pagePartsPlayer = PagePart::where('name', 'GTDL_PLAYER_OF_THE_WEEK')->get();
        $GTDL_PLAYER_OF_THE_WEEK = '';
        if(!$pagePartsPlayer->isEmpty()){
            $GTDL_PLAYER_OF_THE_WEEK = $pagePartsPlayer->first()->html;
        }

        return view('welcome', ['titleEvent' => $titleEvent,
            'welcomeMessage' => $welcomeMessage,
            'GTDL_PLAYER_OF_THE_WEEK' => $GTDL_PLAYER_OF_THE_WEEK]);
    }
}