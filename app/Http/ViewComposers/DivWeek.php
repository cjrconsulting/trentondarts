<?php

namespace TrentonDarts\Http\ViewComposers;

class DivWeek
{
    public $date;
    public $matches;
    public $byeTeamNames = [];

    public function __construct($date, $matches, $byeTeamNames)
    {
        $this->date = $date;
        $this->matches = $matches;
        $this->byeTeamNames = $byeTeamNames;
    }
}