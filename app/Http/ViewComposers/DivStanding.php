<?php

namespace TrentonDarts\Http\ViewComposers;

class DivStanding
{
    public $teamId;
    public $name;
    public $matchPoints;
    public $wonPoints;
    public $lossPoints;
    public $percentage;
    public $ppts;
}