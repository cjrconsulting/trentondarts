<?php

namespace TrentonDarts\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use Illuminate\Database\Eloquent\Collection;

use TrentonDarts\LeagueManagement\Models\WinterSeason;
use TrentonDarts\LeagueManagement\Models\WinterSeasonTeam;

class NextScheduleComposer
{
    public function compose(View $view)
    {
        $season = WinterSeason::where('isCurrent', 1)->firstOrFail();

        $nextWeekSchedules = [];
        $nextWeekDate = $this->getNextWeekDate($season, date('Y-m-d'));
        if (isset($nextWeekDate)) {
            $byeTeamNames = [];
            foreach ($season->teams()->get() as $scheduleTeam) {
                array_push($byeTeamNames, $scheduleTeam->team->name);
            }

            $divWeeks = [];
            $week = $season->weeks()->where('date', date('Y-m-d', strtotime($nextWeekDate)))->first();

            $matches = new Collection([]);
            if (isset($week)) {
                $matches = $week->matches()->orderBy('division')->get();
                foreach ($matches as $match) {
                    $key = array_search($match->homeTeam->name, $byeTeamNames);
                    if ($key !== false) {
                        unset($byeTeamNames[$key]);
                    }
                    $key = array_search($match->awayTeam->name, $byeTeamNames);
                    if ($key !== false) {
                        unset($byeTeamNames[$key]);
                    }
                }
            }

            array_push($divWeeks, new DivWeek($nextWeekSchedules, $matches, $byeTeamNames));
            array_push($nextWeekSchedules, new DivisionSchedule('', $divWeeks));
        }

        $model = [
            'nextWeekDate' => $nextWeekDate,
            'nextWeekSchedules' => $nextWeekSchedules
        ];

        $view->with($model);
    }

    private function getCurrentWeekDate($season, $weekDate)
    {
        if (!$weekDate)
            return null;

        $lower = $season->weeks()->orderBy('date', 'desc')->where('date', '<=', $weekDate)->first();
        if (!$lower)
            return $season->weeks()->orderBy('date', 'asc')->first()->date;

        return $lower->date;
    }

    private function getNextWeekDate($season, $weekDate)
    {
        $upper = $season->weeks()->orderBy('date')->where('date', '>=', $weekDate)->first();
        if (!$upper)
            return null;
        return $upper->date;
    }

    private function getCurrentSeasonPart($season, $weekDate)
    {
        if (!$weekDate) {
            if ($season->weeks()->where('weekType', '=', 'pre')->count() <= 0)
                return 'regular';
        }

        $lower = $season->weeks()->orderBy('date', 'desc')->where('weekType', '<>', 'post')->where('date', '<=', $weekDate)->first();
        if (!$lower)
            return 'pre';
        return $lower->weekType;
    }
}