<?php

namespace TrentonDarts\Http\ViewComposers;

class DivisionSchedule
{
    public $name = '';
    public $weeks;

    public function __construct($name, $divWeeks)
    {
        $this->name = $name;
        $this->weeks = $divWeeks;
    }
}