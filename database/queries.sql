/* Find forfeits for team id */
SELECT a.date, count(*) as games_forfeit
FROM `winter_game_results` join
(
    SELECT `winter_season_matches`.id, CASE WHEN awayTeamId = 4 THEN 'away' ELSE 'home' END AS teamRole, `winter_season_weeks`.date
    FROM `winter_season_matches` join `winter_season_weeks`
        on `winter_season_matches`.weekId = `winter_season_weeks`.id
    WHERE `winter_season_matches`.seasonId = 1 AND (awayTeamId = 4 OR homeTeamId = 4)
) a
    on `winter_game_results`.matchId = a.id
WHERE forfeitedBy = teamRole
GROUP BY a.date