<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsForfeitGameIsAddToStatsTeamGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stats_team_games', function (Blueprint $table) {
            $table->boolean('isForfeitGame')->after('isWon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stats_team_games', function (Blueprint $table) {
            $table->dropColumn('isForfeitGame');
        });
    }
}
