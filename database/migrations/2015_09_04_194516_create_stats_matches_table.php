<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats_matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seasonId');
            $table->string('seasonPart');
            $table->integer('matchId');
            $table->string('division');
            $table->dateTime('date');
            $table->integer('teamId');
            $table->string('teamName');
            $table->integer('pointsWon');
            $table->integer('pointsLost');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_matches');
    }
}
