<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHomeMatchToStatsMatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stats_matches', function (Blueprint $table) {
            $table->boolean('homeMatch')->after('pointsLost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stats_matches', function (Blueprint $table) {
            $table->dropColumn('homeMatch');
        });
    }
}
