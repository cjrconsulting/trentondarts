<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSeasonMatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('season_matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seasonId');
            $table->integer('weekId');
            $table->integer('matchTypeId');
            $table->string('division');
            $table->integer('homeTeamId');
            $table->integer('awayTeamId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('season_matches');
    }
}
