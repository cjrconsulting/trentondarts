<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDartEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dart_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('eventTypeId');
            $table->date('eventDate');
            $table->date('eventEndDate');
            $table->string('dartType');
            $table->string('url');
            $table->string('facebookUrl');
            $table->string('locationName');
            $table->string('address1');
            $table->string('address2');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('registrationStartTime');
            $table->string('registrationEndTime');
            $table->string('dartStart');
            $table->string('mapUrl');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dart_events');
    }
}
