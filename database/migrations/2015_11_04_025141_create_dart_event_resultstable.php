<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDartEventResultstable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dart_event_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('eventId');
            $table->string('specificEventName');
            $table->integer('playerId');
            $table->string('finished');
            $table->integer('orderId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dart_event_results');
    }
}
