<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBoardMembersWithSeasionIds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('board_members', function (Blueprint $table) {
            $table->bigInteger('startSeasonId')->nullable()->after('startingSeason');
            $table->bigInteger('endSeasonId')->nullable()->after('endingSeason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('board_members', function (Blueprint $table) {
            $table->dropColumn('startSeasonId', 'endSeasonId');
        });
    }
}
