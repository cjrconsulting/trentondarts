<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMatchpointsToWinterStatsMatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('winter_stats_matches', function (Blueprint $table) {
            $table->integer('matchPoints')->after('teamName');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('winter_stats_matches', function (Blueprint $table) {
            $table->dropColumn('matchPoints');
        });
    }
}
