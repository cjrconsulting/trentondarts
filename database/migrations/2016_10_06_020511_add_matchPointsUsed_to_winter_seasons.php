<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMatchPointsUsedToWinterSeasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('winter_seasons', function (Blueprint $table) {
            $table->integer('minPointForHalfPoints')->after('defaultMatchTypeId');
            $table->integer('halfPoints')->after('defaultMatchTypeId');
            $table->integer('winPoints')->after('defaultMatchTypeId');
            $table->boolean('isUsingMatchPoints')->after('defaultMatchTypeId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('winter_seasons', function (Blueprint $table) {
            $table->dropColumn('winPoints', 'minPointForHalfPoints', 'halfPoints', 'isUsingMatchPoints');
        });
    }
}
