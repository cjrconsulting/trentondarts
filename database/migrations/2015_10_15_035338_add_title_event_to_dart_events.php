<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleEventToDartEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dart_events', function (Blueprint $table) {
            $table->boolean('isTitleEvent')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dart_events', function (Blueprint $table) {
            $table->dropColumn('isTitleEvent');
        });
    }
}
