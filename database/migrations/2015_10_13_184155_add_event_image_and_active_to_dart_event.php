<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventImageAndActiveToDartEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dart_events', function (Blueprint $table) {
            $table->integer('posterFileId')->after('dartType');
            $table->integer('imageFileId')->after('dartType');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dart_events', function (Blueprint $table) {
            $table->dropColumn('posterFileId');
            $table->dropColumn('imageFileId');
        });
    }
}
