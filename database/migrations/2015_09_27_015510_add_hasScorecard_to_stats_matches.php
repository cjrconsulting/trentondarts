<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasScorecardToStatsMatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stats_matches', function (Blueprint $table) {
            $table->boolean('hasScorecard')->after('homeMatch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stats_matches', function (Blueprint $table) {
            $table->dropColumn('hasScorecard');
        });
    }
}
