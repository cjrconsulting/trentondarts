<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leagueId')->index();
            $table->integer('userId')->index();
            $table->string('firstName');
            $table->string('lastName');
            $table->string('nickname');
            $table->string('email');
            $table->string('homePhone');
            $table->string('cellPhone');
            $table->string('shirtSize');
            $table->string('address1');
            $table->string('address2');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->boolean('acceptText');
            $table->boolean('acceptEmail');
            $table->text('notes');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('players');
    }
}
