<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchTypeGameRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_type_game_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('matchTypeId');
            $table->string('gameType');
            $table->boolean('doubleIn');
            $table->boolean('doubleOut');
            $table->integer('orderId');
            $table->integer('bestOfNumberOfLegs');
            $table->integer('numberOfLegs');
            $table->string('whoStarts', 1);
            $table->integer('numberOfPlayers');
            $table->integer('gamePointValue');
            $table->integer('legPointValue');
            $table->boolean('forfeitIfNoPlayers');
            $table->string('groupName');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_type_game_rules');
    }
}
