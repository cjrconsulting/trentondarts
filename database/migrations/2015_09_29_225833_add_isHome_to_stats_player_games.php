<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsHomeToStatsPlayerGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stats_player_games', function (Blueprint $table) {
            $table->integer('gameNumber')->after('isForfeit');
            $table->boolean('isHome')->after('isForfeit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stats_player_games', function (Blueprint $table) {
            $table->dropColumn('isHome');
            $table->dropColumn('gameNumber');
        });
    }
}
