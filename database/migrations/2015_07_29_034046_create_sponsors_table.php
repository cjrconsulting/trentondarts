<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leagueId')->index();
            $table->string('name');
            $table->string('type', 1);
            $table->string('contactName');
            $table->string('address1');
            $table->string('address2');
            $table->string('city');
            $table->string('state', 2);
            $table->string('zip', 10);
            $table->string('phone', 25);
            $table->string('url');
            $table->string('facebookUrl');
            $table->string('email');
            $table->string('mapUrl');
            $table->text('description');
            $table->text('comments');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sponsors');
    }
}
