<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToStatsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stats_player_games', function (Blueprint $table) {
            $table->index('gameId');
            $table->index('teamId');
            $table->index('playerId');
        });

        Schema::table('stats_team_games', function (Blueprint $table) {
            $table->index('gameId');
            $table->index('teamId');
        });

        Schema::table('stats_matches', function (Blueprint $table) {
            $table->index('matchId');
            $table->index('teamId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stats_player_games', function (Blueprint $table) {
            $table->dropIndex('stats_player_games_gameId_index');
            $table->dropIndex('stats_player_games_teamId_index');
            $table->dropIndex('stats_player_games_playerId_index');
        });

        Schema::table('stats_team_games', function (Blueprint $table) {
            $table->dropIndex('stats_team_games_gameId_index');
            $table->dropIndex('stats_team_games_teamId_index');
        });

        Schema::table('stats_matches', function (Blueprint $table) {
            $table->dropIndex('stats_matches_matchId_index');
            $table->dropIndex('stats_matches_teamId_index');
        });
    }
}
