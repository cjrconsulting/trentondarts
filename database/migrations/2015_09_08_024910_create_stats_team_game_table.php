<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsTeamGameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats_team_games', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seasonId');
            $table->string('seasonPart');
            $table->integer('matchId');
            $table->string('division');
            $table->integer('gameId');
            $table->dateTime('date');
            $table->integer('teamId');
            $table->string('teamName');
            $table->string('gameType');
            $table->integer('numberOfPlayers');
            $table->integer('numberOfPoints');
            $table->boolean('isWon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_team_games');
    }
}
