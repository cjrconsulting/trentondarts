<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsAwardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats_awards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seasonId');
            $table->string('seasonPart');
            $table->integer('matchId');
            $table->string('division');
            $table->integer('gameId');
            $table->dateTime('date');
            $table->integer('teamId');
            $table->string('teamName');
            $table->integer('playerId');
            $table->string('playerName');
            $table->integer('awardId');
            $table->string('awardType');
            $table->integer('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_awards');
    }
}
