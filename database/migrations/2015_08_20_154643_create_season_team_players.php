<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonTeamPlayers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('season_team_players', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leagueId');
            $table->integer('seasonId');
            $table->integer('seasonTeamId');
            $table->integer('playerId');
            $table->string('role');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('season_team_players');
    }
}
