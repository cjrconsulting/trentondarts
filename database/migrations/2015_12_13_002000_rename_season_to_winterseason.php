<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameSeasonToWinterseason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('seasons', 'winter_seasons');
        Schema::rename('season_matches', 'winter_season_matches');
        Schema::rename('season_player_payments', 'winter_season_player_payments');
        Schema::rename('season_team_payments', 'winter_season_team_payments');
        Schema::rename('season_team_players', 'winter_season_team_players');
        Schema::rename('season_teams', 'winter_season_teams');
        Schema::rename('season_weeks', 'winter_season_weeks');

        Schema::rename('game_awards', 'winter_game_awards');
        Schema::rename('game_results', 'winter_game_results');
        Schema::rename('match_results', 'winter_match_results');

        Schema::rename('stats_awards', 'winter_stats_awards');
        Schema::rename('stats_matches', 'winter_stats_matches');
        Schema::rename('stats_player_games', 'winter_stats_player_games');
        Schema::rename('stats_team_games', 'winter_stats_team_games');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('winter_seasons', 'seasons');
        Schema::rename('winter_season_matches', 'season_matches');
        Schema::rename('winter_season_player_payments', 'season_player_payments');
        Schema::rename('winter_season_team_payments', 'season_team_payments');
        Schema::rename('winter_season_team_players', 'season_team_players');
        Schema::rename('winter_season_teams', 'season_teams');
        Schema::rename('winter_season_weeks', 'season_weeks');

        Schema::rename('winter_game_awards', 'game_awards');
        Schema::rename('winter_game_results', 'game_results');
        Schema::rename('winter_match_results', 'match_results');

        Schema::rename('winter_stats_awards', 'stats_awards');
        Schema::rename('winter_stats_matches', 'stats_matches');
        Schema::rename('winter_stats_player_games', 'stats_player_games');
        Schema::rename('winter_stats_team_games', 'stats_team_games');
    }
}
