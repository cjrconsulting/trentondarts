<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlayerPositionToPlayerStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stats_player_games', function (Blueprint $table) {
            $table->integer('playerPosition')->after('playerName');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stats_player_games', function (Blueprint $table) {
            $table->dropColumn('playerPosition');
        });
    }
}
