<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHostColumnsToDartEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dart_events', function (Blueprint $table) {
            $table->string('eventContact2')->after('name');
            $table->string('eventContact')->after('name');
            $table->string('hostPhone')->after('facebookUrl');
            $table->string('hostUrl')->after('facebookUrl');
            $table->string('hostName')->after('facebookUrl');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dart_events', function (Blueprint $table) {
            $table->dropColumn('eventContact');
            $table->dropColumn('eventContact2');
            $table->dropColumn('hostPhone');
            $table->dropColumn('hostUrl');
            $table->dropColumn('hostName');
        });
    }
}
