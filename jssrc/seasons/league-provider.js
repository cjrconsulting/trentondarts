import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import 'fetch';

export class LeagueProvider {
    static inject = [HttpClient];

    constructor(http) {
        this.http = http;
    }

    getTeams() {
        return [
            {
                id: 1,
                name: "Team 1"
            },
            {
                id: 2,
                name: "Team 2"
            }
        ];
    }
}