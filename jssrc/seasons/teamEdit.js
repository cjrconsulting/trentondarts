import {DialogController} from 'aurelia-dialog';
import {LeagueProvider} from 'seasons/league-provider';

export class SeasonTeamEdit {
    static inject = [DialogController, LeagueProvider];

    constructor(controller, leagueProvider) {
        this.controller = controller;
        this.leagueProvider = leagueProvider;
    }

    activate(team){
        this.team = team;
        this.leagueTeams = this.leagueProvider.getTeams();
    }
}