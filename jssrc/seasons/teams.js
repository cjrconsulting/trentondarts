import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {SeasonProvider} from 'seasons/season-provider';
import * as LogManager from 'aurelia-logging';
import {DialogService} from 'aurelia-dialog';
import {SeasonTeamEdit} from 'seasons/teamEdit';

const logger = LogManager.getLogger('season-management');

export class Teams {
    static inject = [Router, SeasonProvider, DialogService];

    teams = [];
    team = { leagueTeamId: 0, preDiv: "2", regularDiv: "" };

    constructor(router, seasonProvider, dialogService){
        this.router = router;
        this.seasonProvider = seasonProvider;
        this.dialogService = dialogService;
    }

    activate(params){
        this.season = this.seasonProvider.getSeasonById(params.id);
        this.teams = this.seasonProvider.getTeamsBySeasonId(params.id);
    }

    addTeam(){
        logger.debug('submitting for dialog.');
        this.dialogService.open({ viewModel: SeasonTeamEdit, model: this.team}).then(() => {
            console.log(this.team);
            console.log('good');
        }).catch(() => {
            console.log('bad');
        });
    }
}