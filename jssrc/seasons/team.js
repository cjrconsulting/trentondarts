import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {SeasonProvider} from 'seasons/season-provider';
import * as LogManager from 'aurelia-logging';

const logger = LogManager.getLogger('season-management');

export class SeasonTeam {
    static inject = [Router, SeasonProvider];

    constructor(router, seasonProvider) {
        this.router = router;
        this.seasonProvider = seasonProvider;
    }

    activate(params) {
        this.season = this.seasonProvider.getSeasonById(params.seasonId);
        this.team = this.seasonProvider.getTeamById(params.id);
    }
}