import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {SeasonProvider} from 'seasons/season-provider';
import * as LogManager from 'aurelia-logging';

const logger = LogManager.getLogger('season-management');

export class Season {
    static inject = [Router, SeasonProvider];
    name = '';
    copyPreviousYearTeams = false;
    isCurrent = false;

    constructor(router, seasonProvider){
        this.router = router;
        this.seasonProvider = seasonProvider;
    }

    submit(){
        logger.info('trying to submit');
        this.seasonProvider.create(
            {
                name: this.name,
                startYear: this.startYear,
                endYear: this.endYear,
                seasonType: 'Winter Season',
                isCurrent: this.isCurrent
            }).then(response =>{
                this.router.navigate('seasons');
            });
    }
}