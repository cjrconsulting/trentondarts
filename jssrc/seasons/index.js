import {inject} from 'aurelia-framework';
import {SeasonProvider} from 'seasons/season-provider';
import 'fetch';

export class Seasons{
    static inject = [SeasonProvider];
    heading = 'Seasons';
    seasons = [];

    constructor(seasonProvider){
        this.seasonProvider = seasonProvider;
    }

    activate(){
        this.seasonProvider.getSeasons()
            .then(response => {
                if(response.statusCode == 200)
                    this.seasons = JSON.parse(response.response);
            });
    }
}
