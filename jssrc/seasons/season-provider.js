import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-http-client';
import 'fetch';

export class SeasonProvider {
    static inject = [HttpClient];
    get seasons() {
        return [
            {
                id: 2,
                name: 'Winter 2014 - 2015',
                startYear: 2014,
                endYear: 2015,
                teams: [
                    {
                        id: 1,
                        team: { id: 1, name: 'Team 1' },
                        preDiv: '1',
                        regularDiv: '',
                        players: [
                            {
                                id: 1,
                                member: { id: 1, name: 'Player Name 1' },
                                role: 'player'
                            },
                            {
                                id: 3,
                                member: { id: 3, name: 'Player Name 2' },
                                role: 'captain'
                            }
                        ]
                    },
                    {
                        id: 2,
                        team: { id: 2, name: 'Team 2'},
                        preDiv: '2',
                        regularDiv: '',
                        players: [
                            {
                                id: 2,
                                member: {id: 2, name: 'Player Name 2' },
                                role: 'player'
                            }
                        ]
                    },
                    {
                        id: 3,
                        team: { id: 5, name: 'Team 3'},
                        preDiv: '2',
                        regularDiv: ''
                    }
                ]
            },
            {
                id: 1,
                name: 'Winter 2013 - 2014',
                startYear: 2013,
                endYear: 2014,
                teams: [
                    {
                        id: 4,
                        team: { id: 1, name: 'Team 1'},
                        preDiv: '1',
                        regularDiv: ''
                    },
                    {
                        id: 5,
                        team: { id: 2, name: 'Team 2'},
                        preDiv: '2',
                        regularDiv: ''
                    },
                    {
                        id: 6,
                        team: { id: 5, name: 'Team 3'},
                        preDiv: '2',
                        regularDiv: ''
                    }
                ]
            }
        ];
    }

    constructor(http) {
        var token = document.querySelector("meta[name=csrf-token]").getAttribute("content");
        this.http = http.configure(x=>{
            x.withHeader("X-CSRF-Token", token);
        });
    }

    create(season){
        return this.http.post("/api/season", season);
    }

    getSeasons(){
        return this.http.get('/api/season');
    }

    getSeasonById(id) {
        return this.seasons.find(function(element){
            return element.id == id;
        });
    }

    getTeamsBySeasonId(id) {
        return this.seasons.find(function(element) {
            return element.id == id;
        }).teams;
    }

    getTeamById(id){
        let season = this.seasons.find(function(season){
            return season.teams.find(function(team){
                return team.id == id;
            }) !== undefined;
        });

        return season.teams.find(function(team){
            return team.id == id;
        });
    }
}