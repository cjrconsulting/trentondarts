import {inject} from 'aurelia-framework';
import {SeasonProvider} from 'seasons/season-provider';

export class EditSeason {
    static inject = [SeasonProvider];

    constructor(seasonProvider){
        this.seasonProvider = seasonProvider;
    }

    activate(params){
        this.id = params.id;
        this.season = this.seasonProvider.getSeasonById(params.id);
        console.info(this.season);
    }
}