import 'bootstrap';
import 'bootstrap/css/bootstrap.css!';

export class App {
  configureRouter(config, router){
    config.title = 'Darts';
    config.map([
      { route: ['','welcome'],  name: 'welcome',       moduleId: 'welcome',      nav: true,  title:'Welcome' },
      { route: 'users',         name: 'users',         moduleId: 'users',        nav: true,  title:'Github Users' },
      { route: 'seasons',       name: 'seasons',       moduleId: 'seasons/index', nav: true, title:'Seasons' },
      { route: 'seasons/create', name: 'createSeason', moduleId: 'seasons/create',           title: 'Season' },
      { route: 'seasons/:id',   name: 'seasonEdit',    moduleId: 'seasons/edit',           title: 'Season' },
      { route: 'seasons/:id/teams',    name: 'seasonTeams', moduleId: 'seasons/teams',        title: 'Season Teams' },
      { route: 'seasons/:seasonId/teams/:id',    name: 'seasonTeamEdit', moduleId: 'seasons/team',  title: 'Season Team' }

    ]);

    this.router = router;

  }
}