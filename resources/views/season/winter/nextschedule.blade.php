<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
        @if(isset($nextWeekDate))
            <h3 class="panel-title">Schedule {{ date('m-d-Y', strtotime($nextWeekDate)) }}</h3>
        @else
            <h4>No Available Schedule at this Time</h4>
        @endif
        </h3>
    </div>
    @if(isset($nextWeekSchedules))
    <table class="table table-striped table-hover">
        <tbody>
          @foreach($nextWeekSchedules as $schedule)
              @foreach($schedule->weeks as $week)
                  @if($week->matches->isEmpty())
                      <tr class="warning"><td colspan="3">Currently No Games</td></tr>
                  @else
                      @foreach($week->matches as $match)
                          <tr>
                              <td style="text-align: right; vertical-align: middle;"><a href="{{route('team.show', ['id' => $match->awayTeamId])}}">{{ $match->awayTeam->name }}</a></td>
                              <td style="width: 10px; text-align: center; vertical-align: middle; font-weight: bold;">@</td>
                              <td style=" vertical-align: middle; font-weight: bold;"><a href="{{route('team.show', ['id' => $match->homeTeamId])}}">{{ $match->homeTeam->name }}</a></td>
                          </tr>
                      @endforeach
                      @if(sizeof($week->byeTeamNames) > 0)
                          <tr>
                              <td colspan="3"><strong>Bye:</strong> <i>{{ join(', ', $week->byeTeamNames) }}</i></td>
                          </tr>
                      @endif
                  @endif
              @endforeach
          @endforeach
        </tbody>
    </table>
    @endif
</div>