@extends('layouts.master')

@section('content')

<!-- Begin copy from fill.html----------------------------------------------- -->
<h1 align="center">
2024-2025 Greater Trenton Dart League <br />
Board of Directors </h1>

<!-- Begin copy from fill.html----------------------------------------------- -->

<table align="center" style="width: 80%" class="table table-bordered table-condensed">
    <thead>
    <tr valign="center">
        <th scope="col"><p>Name</p></th>
        <th scope="col"><p>Position</p></th>
        <th scope="col"><p>Started in Position</p></th>
        <th scope="col"><p>Current Status or End Date</p></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>John Meade</td>
        <td><a link="mailto:president@trentondarts.com">President</a></td>
        <td>2024-2025</td>
        <td>Currently in Position</td>
    </tr>
    <tr>
        <td></td>
        <td>Vice-President</td>
        <td></td>
        <td>Vacant</td>
    </tr>
    <tr>
        <td>Debbie Goodkin</td>
        <td><a link="mailto:events@trentondarts.com">Special Events Coordinator</a></td>
        <td>2021-2022</td>
        <td>Currently in Position</td>
    </tr>
    <tr>
        <td>Matt Hiatt</td>
        <td>Statistician</td>
        <td>2023-2024</td>
        <td>Currently in Position</td>
    </tr>
    <tr>
        <td>Tom Bernhard</td>
        <td>Secretary</td>
        <td>2024-2025</td>
        <td>Currently in Position</td>
    </tr>
    <tr>
        <td>John McManimon</td>
        <td>Officer</td>
        <td>2024-2025</td>
        <td>Currently in Position</td>
    </tr>
    <tr>
        <td>Jimmy Wyckoff</td>
        <td>B Div Officer</td>
        <td>2023-2024</td>
        <td>Currently in Position</td>
    </tr>
    <!--<tr>
        <td>Nick Schneider</td>
        <td>Singles League Coordinator</td>
        <td>2019-2020</td>
        <td>Currently in Position</td>
    </tr>-->
    <tr>
        <td>John Meade</td>
        <td><a link="mailto:webmaster@trentondarts.com">Webmaster</a></td>
        <td>2015-2016</td>
        <td>Currently in Position</td>
    </tr><tr>
        <td>Jeff Brozena</td>
        <td>Previous President</td>
        <td>2020-2023</td>
        <td>Previous Year</td>
    </tr>
    </tbody>
</table>

<div style="text-align:center">
Anyone interested in running for a board position should email Board@trentondarts.com<br/>
All are 2 year terms except for Sec/Treas which will be 3 years to get back on cycle.
</div>

<div style="text-align:center">
If you are interested in running for one of these positions,
please see  <a href="https://www.trentondarts.com/documents/static/gtdlrules.pdf">GTDL Rules</a>
</div>
@stop
