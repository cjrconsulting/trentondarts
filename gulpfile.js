'use strict';
var gulp = require('gulp');
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
require('./eslint-extension');
var eslint = require('gulp-eslint');
var notify = require('gulp-notify');

elixir(function(mix) {
    //mix.eslint(['./resources/assets/js/**/*.js'
    //       ,'./resources/assets/js/**/*.jsx'],
    //  { config: 'eslint.config.json' });

    mix.sass('app.scss');
    mix.browserify('manage.js', 'public/js/manage-bundle.js');

    gulp.src(['./resources/assets/js/**/*.js'
        ,'./resources/assets/js/**/*.jsx'])
        // eslint() attaches the lint output to the eslint property
        // of the file object so it can be used by other modules.
        .pipe(eslint({ config: 'eslint.config.json' }))
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failOnError last.
        .pipe(eslint.failOnError())
        .on('error', function (e) {
            notify.onError({
                title: this.title,
                message: 'ESLint Failed!' + ': <%= error.message %>',
                icon: __dirname + '/../../icons/fail.png'
            })(e);
            this.emit('end');
        });
});

// all gulp tasks are located in the ./build/tasks directory
// gulp configuration is in files in ./build directory
// require('require-dir')('build/tasks');
